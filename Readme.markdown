suddenly
================================================================================
![image](https://img.shields.io/pypi/v/suddenly)
![image](https://img.shields.io/pypi/pyversions/suddenly)
![image](https://gitlab.com/yassu/suddenly/badges/master/pipeline.svg)
![image](https://img.shields.io/pypi/l/suddenly)

```
$ suddenly "突然の死"                                                                                                                      0
＿人人人人人人＿
＞　突然の死　＜
￣Y^Y^Y^Y^Y^Y^￣

```

# How to install

```
$ pip install suddenly
```
