import term
from invoke import task


def print_title(title: str) -> None:
    term.writeLine('running ' + title, term.green)


def _run_commands(ctx, commands):
    for command in commands:
        print(command)
        ctx.run(command)


@task(aliases=['f'])
def format(ctx):
    print_title('yapf')
    ctx.run('yapf -i **/*.py')
    print_title('isort')
    ctx.run('isort **/*.py')


@task(aliases=['t'])
def test(ctx):
    print_title('pytest')
    ctx.run('pytest -vv --cov=suddenly tests')
    print_title('pytest with cov-report')
    ctx.run('pytest --cov=suddenly tests --cov-report=html > /dev/null 2>&1')
    print_title('mypy')
    ctx.run('mypy .')
    print_title('flake8')
    ctx.run('flake8 *.py **/*.py')
    print_title('isort')
    ctx.run('isort --check-only suddenly/ tests/')


@task(test)
def release(ctx):
    _run_commands(ctx, [
        'poetry install',
        'poetry publish --build',
        'git push origin HEAD',
    ])
