from contextlib import redirect_stdout
from io import StringIO

import pytest

from suddenly.suddenly import get_suddenly_text
from suddenly.suddenly import main as _suddenly_main

GET_SUDDENLY_TEXT_PARAMS = [
    ('abc', '＿人人人人人＿\n' + '＞　abc　＜\n' + '￣Y^Y^Y^Y^Y^￣'),
    ('abc\nabc def', '＿人人人人人人人人人＿\n' + '＞　abc　＜\n' + '＞　abc def　＜\n' + '￣Y^Y^Y^Y^Y^Y^Y^Y^Y^￣'),
]


@pytest.mark.parametrize('text, expect', GET_SUDDENLY_TEXT_PARAMS)
def test_get_suddenly_text(text, expect):
    assert get_suddenly_text(text) == expect


def test_main_in_texts_empty_case():
    with pytest.raises(SystemExit):
        _suddenly_main([])


MAIN_OUTPUT_COUNTS_PARAMS = [
    (['abc'], 1),
    (['abc\ndef'], 1),
    (['abc', 'def'], 2),
]


@pytest.mark.parametrize('argv, count_', MAIN_OUTPUT_COUNTS_PARAMS)
def test_main_output_count(argv, count_):
    out_io = StringIO()
    with redirect_stdout(out_io):
        _suddenly_main(argv)
    out_output = out_io.getvalue()
    assert out_output.count('人＿') == count_
